#! /usr/bin/env python3


import sys
import argparse
import json


def vote_sort_key(vote):
    return vote["date"]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('participants', help = 'path of json file containing voters')
    parser.add_argument('article_link', help = 'url of article')
    parser.add_argument('votes', help = 'path of generated json file containing votes')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
    args = parser.parse_args()

    participants_file = open(args.participants)
    data = json.load(participants_file)

    participants = data ["participants"]

    article_votes = []
    for participant in participants:
        assert len(participant) == 1
        name, participant_info = list(participant.items())[0]

        for vote in participant_info["votes"]:
            if vote["link"] == args.article_link:
                article_votes.append(vote)
    article_votes.sort(key = vote_sort_key)

    votes_file = open(args.votes, "w")
    json.dump(article_votes, votes_file, ensure_ascii = False, indent = 2)


    print(len(article_votes))

    return 0


if __name__ == '__main__':
    sys.exit(main())
